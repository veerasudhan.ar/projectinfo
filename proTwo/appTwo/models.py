from django.db import models
from django.http import request

# Create your models here.
class usersdata(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    email = models.EmailField(max_length=264,unique=True)