from django import forms
from appTwo.models import usersdata

class NewUser(forms.ModelForm):
    class Meta():
        model = usersdata
        fields = '__all__'