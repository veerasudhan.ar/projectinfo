from django.shortcuts import render
#from appTwo.models import usersdata
from appTwo.forms import NewUser
# Create your views here.

def index(request):
    return render(request,'appTwo/index.html')


def users(request):
    form =NewUser()
    if request.method == "POST":
        form = NewUser(request.POST)

        if form.is_valid():
            form.save(commit=True)
            return index(request)

        else:
            print('ERROR FORM INVALID')
    return render(request,'appTwo/user.html',{ 'form' : form })

